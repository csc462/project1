import socket
from optparse import OptionParser
from sys import exit

parser = OptionParser()
parser.add_option("-o", "--operation", dest="operation",
                  help="GET, PUT, DELETE or CREATE_TABLE")
parser.add_option("-k", "--key", dest="key",
                  help="Key for distributed hash table")
parser.add_option("-v", "--value", dest="value",
                  help="Value for distributed hash table")

(opts, args) = parser.parse_args()
  
operation = opts.operation
key = opts.key
value = opts.value

if not(operation == "GET" or operation == "PUT" or operation == "DELETE" or operation == "SHUTDOWN"):
	print "ERROR: Operation " + operation + " not found."
	sys.exit()

s = socket.socket() # make a socket
host = '10.0.0.253' # coordinator IP
port = 12345 # use a port that won't be used
s.connect((host, port))

command = operation
if(not(operation == "SHUTDOWN")):
	command += " " + key
if(operation == "PUT"):
	command = command + ":" + value

s.send(command)

if(not(operation == "SHUTDOWN")):
	value = ''
	while True:
		value = s.recv(1024)
		if value:
			break
	print value

s.close()