from fabric.api import *
from StringIO import StringIO
from optparse import OptionParser
import sqlite3 as lite
import threading
import Queue
import socket
import time
import datetime
import select
import re

env.key_filename="./id_rsa"
env.use_ssh_config = True
env.ssh_config_path = './ssh-config'

replicas = ["slice328.pcvm3-1.instageni.metrodatacenter.com", "slice328.pcvm2-2.instageni.rnoc.gatech.edu",
               "slice328.pcvm3-2.instageni.illinois.edu", "slice328.pcvm5-7.lan.sdn.uky.edu"]
replicas_down = []			   

#stores commands in form (command, address)
#an example would be ('GET fruit', '10.0.0.0')
commandQueue = Queue.Queue(maxsize=0)
#stores values to send back in the form (message, type, sender)
#an example would be ('apple', 'GET', '10.0.0.0')
responseList = []

goodCommands = ["GET", "PUT", "DELETE"]

replica_down = "ERROR: Replica down."

running = True
con = None
dbFile = "Server.db"

parser = OptionParser()
parser.add_option("-o", "--operation", dest="operation",
                  help="CREATE_TABLE")

(opts, args) = parser.parse_args()
  
operation = opts.operation

'''
This method is the thread method for the client threads.
A thread will spawn when a client connects, and this method handles
communication with ONE client. Each client has their own thread.

This method does the following:
1. Receives data from the client
2. Put the command and the IP address of the client in the commandQueue
3. If this is a GET, wait until our command is done parsing
4. If our response is in the responseList, send it back to client and finish processing
'''
def handle(clientsocket, address):
	global running
	#receive data from client
	data = clientsocket.recv(1024)
	type = str.split(data, " ")[0]
	if(type == "SHUTDOWN"):
		running = False
	else:
		#put in command queue to be parsed later
		commandQueue.put((data, address[0]))
		#If we are doing a GET, wait until our request is parsed from queue
		#it will show up in responseList when done. Send value back after
		if(type in goodCommands):
			while True:
				response = next((x for x in responseList if (x[1] == type and x[2] == address[0])),False)
				if(response == False):
					continue
				else:
					responseList.remove(response)
					clientsocket.send(response[0])
					break
	clientsocket.close()

'''
This method is the thread method for the command handling thread.
commandQueue stores tuples in the form (command, address). Address is
the IP address of the client that sent this command. This is stored
so we can store the response along with this so the client thread knows
which response is for itself

What this method does:
1. pop a command off the queue
2. Parse what type of command it is.
3. Requests/sends data from/to one or multiple replicas
4. In the case of GET, stores the value for later in responseList
'''
def doCommand():
	whichReplica = 0
	while running:
		#only do something if we have a command
		if(commandQueue.empty()):
			continue
		command = commandQueue.get()
		#first part of the tuple is the command and args
		args = str.split(command[0], " ")
		#first arg is type of command
		type = args[0]
		if(type == "GET"):
			#second arg is the key. Just iterate thru which replica we
			#want to use
			host = replicas[whichReplica%len(replicas)]
			retVal = execute(our_get, args[1], hosts = [host])
			
			# Handle case of replica being down
			while retVal[host] == replica_down:
				#if len(replicas) == 0:
					# Exit server if there are no more replicas
				#	sys.exit()
				replicas_down.append(host)
				replicas.remove(host)
				print "GET failed, switching replicas"
				host = replicas[whichReplica%len(replicas)]
				retVal = execute(our_get, args[1], hosts = [host])
				
			#return is a dict with hostname as key. Get the value
			value = retVal[host]
			#increment so we use different replica next get
			whichReplica += 1
			#add this to response list so it can be sent back in client thread
			responseList.append((value, type, command[1]))
		elif(type == "PUT"):
			keyvalue = str.split(args[1], ":")
			retVal = execute(our_put, get_transaction_number(), keyvalue[0], keyvalue[1], hosts = replicas)
			replicas_to_remove = []
			value_added = False
			for replica in replicas:
				value = retVal[replica]
				if value == None:
					replicas_down.append(replica)
					replicas_to_remove.append(replica)
					continue
				else:
					if not(value_added):
						value_added = True
						responseList.append((value, type, command[1]))
			for replica in replicas_to_remove:
				replicas.remove(replica)
		elif(type == "DELETE"):
			retVal = execute(our_delete, get_transaction_number(), args[1], hosts = replicas)
			replicas_to_remove = []
			value_added = False
			for replica in replicas:
				value = retVal[replica]
				if value == None:
					replicas_down.append(replica)
					replicas_to_remove.append(replica)
					continue
				else:
					if not(value_added):
						value_added = True
						responseList.append((value, type, command[1]))
			for replica in replicas_to_remove:
				replicas.remove(replica)

# If a replica is down, ping every 5 seconds
def ping_downed_replicas():
	whichReplica = 0;
	while running:
		if len(replicas_down) > 0:
			host = replicas_down[whichReplica%len(replicas_down)]
			alive = execute(ping_replica, hosts = [host])
			if alive[host]:
				replicas.append(host)
				replicas_down.remove(host)

				print "Ping Succeeded: " + host

				restore_replica_db(host)

			else:
				whichReplica = whichReplica + 1;
				print "Ping Failed: " + host
				time.sleep(5) # Ping every 5 seconds
	
def ping_replica():
	with settings(warn_only=True):
		ping_value = run('python DistHash.py -o PING')
		if ping_value.return_code == 2:
			return False
		else:
			return True

@roles('replicas')
def get_logs():
	with settings(warn_only=True):
		fd = StringIO()
		get('log.txt', fd)
		log = fd.getvalue()
		return log

def create_table():
	con = lite.connect(dbFile)
	cur = con.cursor()
	
	cur.execute("CREATE TABLE Hash(key TEXT, value TEXT)")
	print("Table Created")
	con.close()

def get_transaction_number():
	trans = 0;

	con = lite.connect(dbFile)
	cur = con.cursor()
	
	cur.execute("SELECT value FROM Hash WHERE key='transaction'")
	rows = cur.fetchall()
	
	if len(rows) == 0:
		# Add new key ('transaction') to the table
		cur.execute("INSERT INTO Hash VALUES('transaction', '"+str(trans)+"')")
		print ("Transaction number: T" + str(trans))
	else:
		# There will only be one row due to unique keys
		for row in rows:
			#print row[0]
			trans = int(row[0])
		trans = trans + 1
		# Update value of existing transaction number
		cur.execute("UPDATE Hash SET value='"+str(trans)+"' WHERE key='transaction'")
		print ("Transaction number: T" + str(trans))
	
	con.commit()
	con.close()

	return "T" + str(trans)

def restore_replica_db(host):
	most_recent_trans = 0

	con = lite.connect(dbFile)
	cur = con.cursor()
	
	# get most recent transaction nuumber
	cur.execute("SELECT value FROM Hash WHERE key='transaction'")
	rows = cur.fetchall()
	if len(rows) == 0:
		print "ERROR: No transactions found on server."
	else:
		# There will only be one row due to unique keys
		for row in rows:
			most_recent_trans = int(row[0])
	
	con.close()

	replica_down_log = execute(get_logs, hosts = [host])

	# check if log is up to date
	if replica_down_log[host].find('T'+str(most_recent_trans)) == -1:

		print "Replica not up to date"

		# get last transaction
		log_list = replica_down_log[host].split()
		length = len(log_list)
		last_trans = log_list[length - 1]

		# get outstanding transactions
		good_log = execute(get_logs, hosts = replicas[0])
		outstanding_trans = good_log[replicas[0]].split(last_trans)
		outstanding_commands = re.compile("\d+-\d+-\d+ \d+:\d+:\d+ |\n").split(outstanding_trans[1])

		# execute all outstanding transactions
		for command in outstanding_commands:
			if command:
				execute(our_command, command, hosts = [host])

		# check that replica is now up to date
		con = lite.connect(dbFile)
		cur = con.cursor()
		
		# get most recent transaction nuumber
		cur.execute("SELECT value FROM Hash WHERE key='transaction'")
		rows = cur.fetchall()
		if len(rows) == 0:
			print "ERROR: No transactions found on server."
		else:
			# There will only be one row due to unique keys
			for row in rows:
				most_recent_trans = int(row[0])
		
		con.close()

		replica_log = execute(get_logs, hosts = [host])
		if replica_log[host].find('T'+str(most_recent_trans)) == -1:
			print "ERROR: Replica still not up to date"
		else:
			print "Replica now up to date"

	else:
		print "Replica up to date"


#These three methods are just dummy methods called by doCommand.
#The reason behind these is we had to use execute() not run() so we
#could specify the hosts as this isn't a 'fabfile' persay, and execute()
#needs to call another function.
def our_get(key):
	with settings(warn_only=True):
		get_value = run('python DistHash.py -o GET -k ' + key)
		if get_value.return_code == 2:
			return replica_down
		else:
			return get_value
	
	
@parallel
def our_put(trans, key, value):
	with settings(warn_only=True):
		put_value =  run('python DistHash.py -o PUT -k ' + key + ' -v ' + value + ' -t ' + trans)
		if put_value.return_code == 2:
			print replica_down
		else:
			return put_value
	
@parallel
def our_delete(trans, key):
	with settings(warn_only=True):
		delete_value =  run('python DistHash.py -o DELETE -k ' + key + ' -t ' + trans)
		if delete_value.return_code == 2:
			print replica_down
		else:
			return delete_value

@parallel
def our_command(command):
	with settings(warn_only=True):
		delete_value =  run('python DistHash.py -o ' + command)
		if delete_value.return_code == 2:
			print replica_down
		else:
			return delete_value

'''
The main here does the following things:
1. Spawns up a thread to deal with the commands (read doCommand comment for more info)
2. Opens a socket for connections from clients on the server
3. Goes in an infinite loop accepting connections
4. If a client connects, spawn a thread that will send a message to said client.
	(read handle() comment for more info)
'''
if __name__ == '__main__':
	# make the command handler thread
	t = threading.Thread(target=doCommand)
	t.daemon = True
	t.start()

	# for installation, create table to hold tranasaction number
	if operation == "CREATE_TABLE":
		create_table()

	# make the ping failed replicas thread
	ping_thread = threading.Thread(target=ping_downed_replicas)
	ping_thread.daemon = True
	ping_thread.start()
	
	host = socket.gethostname() # get host name
	
	port = 12345 # use a port that won't be used
	serverSocket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
	serverSocket.bind((host, port))
	serverSocket.listen(128)
	read_list = [serverSocket]
	while running:
		# this is so we are't always waiting for a connections
		# changed to use select so we can send a SHUTDOWN command
		readable, writable, errored = select.select(read_list, [], [])
		for s in readable:
			if s is serverSocket:
				(clientsocket, address) = serverSocket.accept()
				clientthread = threading.Thread(target=handle, args=(clientsocket,address))
				clientthread.daemon = True
				clientthread.start()
	print "Server shutting down..."
	serverSocket.close()