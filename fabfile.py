from fabric.api import *
import re
env.hosts = [
  "slice328.pcvm3-1.geni.case.edu",
    "slice328.pcvm3-1.instageni.metrodatacenter.com",
    "slice328.pcvm2-2.instageni.rnoc.gatech.edu",
    "slice328.pcvm3-2.instageni.illinois.edu",
    "slice328.pcvm5-7.lan.sdn.uky.edu",
    "slice328.pcvm3-1.instageni.lsu.edu",
    "slice328.pcvm2-2.instageni.maxgigapop.net",
    "slice328.pcvm1-1.instageni.iu.edu",
    "slice328.pcvm3-4.instageni.rnet.missouri.edu",
    "slice328.pcvm3-7.instageni.nps.edu",
    "slice328.pcvm2-1.instageni.nysernet.org",
    "slice328.pcvm3-11.genirack.nyu.edu",
    "slice328.pcvm5-1.instageni.northwestern.edu",
    "slice328.pcvm5-2.instageni.cs.princeton.edu",
    "slice328.pcvm3-3.instageni.rutgers.edu",
    "slice328.pcvm1-6.instageni.sox.net",
    "slice328.pcvm3-1.instageni.stanford.edu",
    "slice328.pcvm2-1.instageni.idre.ucla.edu",
    "slice328.pcvm4-1.utahddc.geniracks.net",
    "slice328.pcvm1-1.instageni.wisc.edu",
  ]

env.key_filename="./id_rsa"
env.use_ssh_config = True
env.ssh_config_path = './ssh-config'
env.roledefs.update({
    'coordinator':["slice328.pcvm3-1.geni.case.edu"],
    'replicas':["slice328.pcvm3-1.instageni.metrodatacenter.com", "slice328.pcvm2-2.instageni.rnoc.gatech.edu",
               "slice328.pcvm3-2.instageni.illinois.edu", "slice328.pcvm5-7.lan.sdn.uky.edu"],
    'replica1':["slice328.pcvm3-1.instageni.metrodatacenter.com"],
    'replica2':["slice328.pcvm2-2.instageni.rnoc.gatech.edu"],
    'replica3':["slice328.pcvm3-2.instageni.illinois.edu"],
    'replica4':["slice328.pcvm5-7.lan.sdn.uky.edu"],
    'clients':["slice328.pcvm3-1.instageni.lsu.edu",
    "slice328.pcvm2-2.instageni.maxgigapop.net",
    "slice328.pcvm1-1.instageni.iu.edu",
    "slice328.pcvm3-4.instageni.rnet.missouri.edu",
    "slice328.pcvm3-7.instageni.nps.edu",
    "slice328.pcvm2-1.instageni.nysernet.org",
    "slice328.pcvm3-11.genirack.nyu.edu",
    "slice328.pcvm5-1.instageni.northwestern.edu",
    "slice328.pcvm5-2.instageni.cs.princeton.edu",
    "slice328.pcvm3-3.instageni.rutgers.edu",
    "slice328.pcvm1-6.instageni.sox.net",
    "slice328.pcvm3-1.instageni.stanford.edu",
    "slice328.pcvm2-1.instageni.idre.ucla.edu",
    "slice328.pcvm4-1.utahddc.geniracks.net",
    "slice328.pcvm1-1.instageni.wisc.edu"],
    'client':["slice328.pcvm3-1.instageni.lsu.edu"]
})

@roles('replicas')
def pingtest():
	run('ping -c 3 www.yahoo.com')

def uptime():
    run('uptime')

@roles('replicas')
def install():
	run('apt-get -y update')
	run('apt-get -y install sqlite')
	run('sqlite DistHash.db')
	put('DistHash.py')

@parallel
@roles('clients')
def put_client():
	put('client.py')
	
@roles('coordinator')
def put_coord():
	put('server.py')
	
@roles('client')
def shutdown():
	run('python client.py -o SHUTDOWN')

@roles('client')
def test_client_delete():
	run('python client.py -o DELETE -k fruit')

@parallel
@roles('clients')
def test_concurrent_gets():
    run('python client.py -o GET -k fruit')

@parallel
@roles('clients')
def test_concurrent_puts_deletes():
    run('python client.py -o GET -k fruit')
    run('python client.py -o DELETE -k fruit')
    run('python client.py -o PUT -k fruit -v apple')
    run('python client.py -o GET -k fruit')
	
@roles('coordinator')
def run_coord():
	run('python server.py')

@roles('coordinator')
def install_coord():
    run('apt-get -y update')
    run('apt-get -y install fabric')
    run('apt-get -y install sqlite')
    put('server.py')
    put('fabfile.py')
    run('sqlite Server.db')
    run('python server.py -o CREATE_TABLE')

@parallel
@roles('replicas')
def create_table():
	run('python DistHash.py -o CREATE_TABLE')

@roles('replicas')
def test_db():	
	run('python DistHash.py -o PUT -k myKey -v myValue')
	run('python DistHash.py -o PUT -k fruit -v orange')
	run('python DistHash.py -o GET -k myKey')
	run('python DistHash.py -o GET -k fruit')
	run('python DistHash.py -o PUT -k fruit -v apple')
	run('python DistHash.py -o GET -k fruit')
	run('python DistHash.py -o DELETE -k myKey')
	run('python DistHash.py -o DELETE -k fruit')
	
@roles('replicas')
def populate_db():	
	run('python DistHash.py -o PUT -k myKey -v myValue')
	run('python DistHash.py -o PUT -k fruit -v orange')
	run('python DistHash.py -o PUT -k vegetable -v carrot')
	
@roles('replicas')
def put_DistHash():	
	put('DistHash.py')

@roles('replica1')
def stop_rep1():
	run('rm DistHash.py')
	
@roles('replica1')
def start_rep1():
	put('DistHash.py')