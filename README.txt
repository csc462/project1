# RPC-and-Two-Phase-Commit #

This project is written in python and designed to be run on a slicelet allocated from the GENI project. You will require a computer with a shell to run all of this (OSX, Linux)


# Set-Up Instructions #

You first by requesting your GENI slicelet, download it and extract it into your folder for this project. The commands you will run in order are 
fab install
fab put_client
fab put_coord
fab put_DistHash
fab create_table
fab install_coordinator
fab run_coordinator

The next thing you will do, is change the replicas at the top of the server.py file to yours, as they're set to the ones from our slicelet and as a result will not work for others. 

# Test Cases
To perform a 2PC, from your fab file you can try the following test cases:

Test case 1: Concurrently GET from multiple clients
fab run_coord (runs server.py on the coordinator)
fab test_concurrent_gets (runs a GET command from each client in parallel)

Test case 2: Concurrently PUT and DELETE from multiple clients
fab run_coord (runs server.py on the coordinator)
fab test_concurrent_puts_deletes (runs PUT and DELETE commands from each client in parallel)

Test case 3: Single replica failure
fab run_coord (runs server.py on the coordinator)
fab stop_rep1 (simulates a replica failure by removing the DB interface) 
fab test_concurrent_gets (run a command so the coordinator contacts the replicas)

Test case 4: Single replica restart after failure (DB up to date)
fab run_coord (runs server.py on the coordinator)
fab stop_rep1 (simulates a replica failure by removing the DB interface) 
fab test_concurrent_gets (run a command so the coordinator contacts the replicas)
fab start_rep1 (simulates a replica being restarted after failure)

Test case 5: Single replica restart after failure (DB not up to date)
fab run_coord (runs server.py on the coordinator)
fab stop_rep1 (simulates a replica failure by removing the DB interface) 
fab test_concurrent_puts_deletes (puts and deletes some db items)
fab start_rep1 (simulates a replica being restarted after failure)

Test case 6: Multiple replica restart after failure
fab run_coord (runs server.py on the coordinator)
fab stop_rep1 (simulates a replica failure by removing the DB interface) 
fab stop_rep2
fab test_concurrent_puts_deletes (puts and deletes some db items)
fab start_rep1 (simulates a replica being restarted after failure)
fab start_rep2

Test case 7: Coordinator failure
(TBD in future)

Test case 8: Coordinator restart after failure
(TBD in future)

##Project Description##

#Durable key/value store#
* Create ¾ machines that will server as database server
  * Install sqlite on each
* put(k,v)
  * State changed
  * Should use 2PC to coordinate between replicas
* del(k)
  * State changed
  * Should use 2PC to coordinate between replicas
* v = get(k)
  * Can be sent to any replica, should all have the same data
* Has to be “durable”, if project quits all values must be retrievable with the get (same state as before the quit or crash)

# Two-Phase Commit #
* Single coordinator process with multiple replica processes
  * Fabric has roles which can accomplish this
  * One roll could be 'master' and the other roles are 'slaves'

```
#!python
env.roledefs.update({
  'master’':["<master hostname>"],
  'clients':["<client1 hostname>", "<client2 hostname>", etc.]
})
```

Then above functions, use @roles to define which servers RPCs are run on 
* @roles('master')
*  def master_commands():

Fabric RPC commands to use the key/store value and call
* get(k)
* put(k,v)
* delete(k)

Implement Logging
* What to log?
  * Puts and Deletes
  * Failed puts/deletes and gets
  * Gets do not change state so probably don't need to log this

Fault Tolerance
* If master or replicas fail
*  When node that fails recovers, need to be synced with other replicas or master