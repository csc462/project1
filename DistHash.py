import sqlite3 as lite
import time
import datetime
from optparse import OptionParser

def put(k, v, t):
	con = lite.connect(dbFile)
	cur = con.cursor()
	
	cur.execute("SELECT value FROM Hash WHERE key='" +k+"'")
	rows = cur.fetchall()
	
	if len(rows) == 0:
		# New key being added to the table
		cur.execute("INSERT INTO Hash VALUES('"+k+"', '"+v+"')")
		print (k + ":" + v + " inserted.")
	else:
		# Update value of existing key in table
		cur.execute("UPDATE Hash SET value='"+v+"' WHERE key='"+k+"'")
		print (k + ":" + v + " updated.")
	
	logFile.write(get_current_time()+" PUT -k "+k+" -v "+v+" -t "+t+"\n")
	con.commit()
	con.close()

def delete(k, t):
	con = lite.connect(dbFile)
	cur = con.cursor()
	
	cur.execute("SELECT value FROM Hash WHERE key='" +k+"'")
	rows = cur.fetchall()
	
	if len(rows) == 0:
		print("ERROR: Key "+k+" not found.")
	else:
		cur.execute("DELETE FROM Hash WHERE key='" +k+"'")
		print ("Key: " + k + " deleted.")
		logFile.write(get_current_time()+" DELETE -k "+k+" -t "+t+"\n")
	
	con.commit()
	con.close()

def get(k):
	con = lite.connect(dbFile)
	cur = con.cursor()
	
	cur.execute("SELECT value FROM Hash WHERE key='" +k+"'")
	rows = cur.fetchall()
	if len(rows) == 0:
		print "ERROR: Key not found."
	else:
		# There will only be one row due to unique keys
		for row in rows:
			print row[0]
	
	con.close()

def create_table():
	con = lite.connect(dbFile)
	cur = con.cursor()
	
	cur.execute("CREATE TABLE Hash(key TEXT, value TEXT)")
	print("Table Created")
	con.close()
	
def get_current_time():
	current_time = time.time()
	formatted_time = datetime.datetime.fromtimestamp(current_time).strftime('%Y-%m-%d %H:%M:%S')
	return formatted_time
	
con = None
dbFile = "DistHash.db"
log = "log.txt"

parser = OptionParser()
parser.add_option("-o", "--operation", dest="operation",
                  help="GET, PUT, DELETE, PING or CREATE_TABLE")
parser.add_option("-k", "--key", dest="key",
                  help="Key for distributed hash table")
parser.add_option("-v", "--value", dest="value",
                  help="Value for distributed hash table")
parser.add_option("-t", "--transaction", dest="transaction",
                  help="Transaction number of the command committed by coordinator")

(opts, args) = parser.parse_args()
  
operation = opts.operation
key = opts.key
value = opts.value
transaction = opts.transaction

logFile = open(log, "a")

if operation == "CREATE_TABLE":
	create_table()
elif operation == "GET":
	get(key)
elif operation == "PUT":
	put(key, value, transaction)
elif operation == "DELETE":
	delete(key, transaction)
elif operation == "PING":
	print "PING"
else:
	print "ERROR: Operation " + operation + " not found."
	
logFile.close()